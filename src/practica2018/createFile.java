package practica2018;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import practica2018.buton.Action1;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class createFile {
	
	 public static void xml(String string) {

	      try {
	    	  InputStreamReader read = new InputStreamReader(System.in);
	  		  BufferedReader in = new BufferedReader(read);
	    	  String a = new String();
	    	  String myDriver = "com.mysql.jdbc.Driver";
		      String myUrl = "jdbc:mysql://localhost:3306/practica2018";
		      Class.forName(myDriver);
		      Connection conn = DriverManager.getConnection(myUrl, "root", "");
		      //System.out.println("Alegeti tabelul");
		     //a = in.readLine();
		      String query = "SELECT * FROM aeroport";

		  
		      Statement st = conn.createStatement();
		      
		      
		      ResultSet rs = st.executeQuery(query);
	    	 DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.newDocument();
	         
	         // root element
	         Element rootElement = doc.createElement("practica2018");
	         doc.appendChild(rootElement);
	         while (rs.next()) {
	         // supercars element
	         Element supercar = doc.createElement("tabel");
	         rootElement.appendChild(supercar);

	         // setting attribute to element
	         Attr attr = doc.createAttribute("aeroport");
	         attr.setValue("_");
	         supercar.setAttributeNode(attr);

	         Element carname1 = doc.createElement("Coloana");
	         Attr attrType1 = doc.createAttribute("_");
	         attrType1.setValue("NumeAeroport");
	         carname1.setAttributeNode(attrType1);
	         carname1.appendChild(doc.createTextNode(rs.getString("NumeAeroport")));
	         supercar.appendChild(carname1);

	         Element carname2 = doc.createElement("Coloana");
	         Attr attrType2 = doc.createAttribute("_");
	         attrType2.setValue("AvioaneSosire");
	         carname2.setAttributeNode(attrType2);
	         carname2.appendChild(doc.createTextNode(rs.getString("AvioaneSosire")));
	         supercar.appendChild(carname2);
	         
	         Element carname3 = doc.createElement("Coloana");
	         Attr attrType3 = doc.createAttribute("_");
	         attrType3.setValue("AvioaneDecolare");
	         carname3.setAttributeNode(attrType3);
	         carname3.appendChild(doc.createTextNode(rs.getString("AvioaneDecolare")));
	         supercar.appendChild(carname3);

	         }
	         st.close();
	         // write the content into xml file
	         TransformerFactory transformerFactory = TransformerFactory.newInstance();
	         Transformer transformer = transformerFactory.newTransformer();
	         DOMSource source = new DOMSource(doc);
	         StreamResult result = new StreamResult(new File("aeroport.xml"));
	         transformer.transform(source, result);
	         
	         // Output to console for testing
	         StreamResult consoleResult = new StreamResult(System.out);
	         transformer.transform(source, consoleResult);
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }

	public static void main (String[] args){    
		  JFrame frame = new JFrame("Baza de date");
		  frame.setVisible(true);
		  frame.setSize(500,200);
		  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		  JPanel panel = new JPanel();
		  frame.add(panel);
		  JButton button = new JButton("Generare XML");
		  panel.add(button);
		  button.addActionListener (new Action1());
		}
		static class Action1 implements ActionListener {        
		  public void actionPerformed (ActionEvent e) {     
			  JFrame frame = new JFrame("Baza de date");
			  frame.setVisible(true);
			  frame.setSize(500,200);
			  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			  JPanel panel = new JPanel();
			  frame.add(panel);
			  
			  JButton button = new JButton("Generare XML aeroport");
			  panel.add(button);
			  button.addActionListener (new Action2());
		  }
		}
		static class Action2 implements ActionListener {        
			  public void actionPerformed (ActionEvent e) {     
			    JFrame frame3 = new JFrame("Generare XML aeroport");
			    frame3.setVisible(true);
			    frame3.setSize(200,200);
			    xml("aeroport");
			    JLabel label = new JLabel("Fisierul XML a fost generat");
			    JPanel panel = new JPanel();
			    frame3.add(panel);
			    panel.add(label);
			  }
			} 

	
}
