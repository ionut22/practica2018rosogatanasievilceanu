package practica2018;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class createFile1 {

   public static void main(String argv[]) {

      try {
    	  InputStreamReader read = new InputStreamReader(System.in);
  		  BufferedReader in = new BufferedReader(read);
    	  String a = new String();
    	  String myDriver = "com.mysql.jdbc.Driver";
	      String myUrl = "jdbc:mysql://localhost:3306/practica2018";
	      Class.forName(myDriver);
	      Connection conn = DriverManager.getConnection(myUrl, "root", "");
	      //System.out.println("Alegeti tabelul");
	     //a = in.readLine();
	      String query = "SELECT * FROM avion";

	  
	      Statement st = conn.createStatement();
	      
	      
	      ResultSet rs = st.executeQuery(query);
    	 DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.newDocument();
         
         // root element
         Element rootElement = doc.createElement("practica2018");
         doc.appendChild(rootElement);
         
         while (rs.next()) {
         
        	 // supercars element
         Element supercar = doc.createElement("tabel");
         rootElement.appendChild(supercar);

         	// setting attribute to element
         Attr attr = doc.createAttribute("avion");
         attr.setValue("_");
         supercar.setAttributeNode(attr);

         // carname element
        /* Element carname = doc.createElement("Coloana");
         Attr attrType = doc.createAttribute("_");
         attrType.setValue("ID");
         carname.setAttributeNode(attrType);
         carname.appendChild(doc.createTextNode(rs.getString("Id")));
         supercar.appendChild(carname);*/

         Element carname1 = doc.createElement("Coloana");
         Attr attrType1 = doc.createAttribute("_");
         attrType1.setValue("NumeAvion");
         carname1.setAttributeNode(attrType1);
         carname1.appendChild(doc.createTextNode(rs.getString("NumeAvion")));
         supercar.appendChild(carname1);

         Element carname2 = doc.createElement("Coloana");
         Attr attrType2 = doc.createAttribute("_");
         attrType2.setValue("CapacitateaAvionului");
         carname2.setAttributeNode(attrType2);
         carname2.appendChild(doc.createTextNode(rs.getString("CapacitateaAvionului")));
         supercar.appendChild(carname2);
         
         Element carname3 = doc.createElement("Coloana");
         Attr attrType3 = doc.createAttribute("_");
         attrType3.setValue("GreutateaAvionului");
         carname3.setAttributeNode(attrType3);
         carname3.appendChild(doc.createTextNode(rs.getString("GreutateaAvionului")));
         supercar.appendChild(carname3);
         

         }
         st.close();
         	// write the content into xml file
         TransformerFactory transformerFactory = TransformerFactory.newInstance();
         Transformer transformer = transformerFactory.newTransformer();
         DOMSource source = new DOMSource(doc);
         StreamResult result = new StreamResult(new File("avion.xml"));
         transformer.transform(source, result);
         
         	// Output to console for testing
         StreamResult consoleResult = new StreamResult(System.out);
         transformer.transform(source, consoleResult);
      } catch (Exception e) {
         e.printStackTrace();
        }
   }
}
