package practica2018;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;

public class buton {

	public static void xml(String tabel) {
		 try {
	    	 String q1 = new String();
	    	 String q2 = new String();
	    	 String q3 = new String();
	    	 String q4 = new String();
	    	  //String a = new String();
	    	  String myDriver = "com.mysql.jdbc.Driver";
		      String myUrl = "jdbc:mysql://localhost:3306/practica2018";
		      Class.forName(myDriver);
		      Connection conn = DriverManager.getConnection(myUrl, "root", "");
		      String query = "SELECT * FROM " + tabel;

		  
		      Statement st = conn.createStatement();
		      
		      
		      ResultSet rs = st.executeQuery(query);
	    	 DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.newDocument();
	         
	         
	        	 if(Objects.equals("avion", new String(tabel))) {
	        		 q1 = "NumeAvion";
		        	 q2 = "CapacitateaAvionului";
		        	 q3 = "GreutateaAvionului";
	        	 }else {
	        		 if(Objects.equals("bilet", new String(tabel))) {
		        		 q1 = "ora";
			        	 q2 = "minut";
			        	 q3 = "secunda";
		        	 }else {
		        		 if(Objects.equals("ruta", new String(tabel))) {
			        		 q1 = "NumeRuta";
				        	 q2 = "DurataZboruluiMinute";
				        	 q3 = "DurataZboruluiKM";
			        	 }
		        	 }
	        	 }
	         
	         
	         
	         // root element
	         Element rootElement = doc.createElement("test");
	         doc.appendChild(rootElement);
	         while (rs.next()) {
	         // supercars element
	         Element supercar = doc.createElement(tabel);
	         rootElement.appendChild(supercar);

	         // setting attribute to element
	         Attr attr = doc.createAttribute("aeroport");
	         attr.setValue("_");
	         supercar.setAttributeNode(attr);

	         // carname element
	         

	         Element carname1 = doc.createElement("Coloana");
	         Attr attrType1 = doc.createAttribute("_");
	         attrType1.setValue(q1);
	         carname1.setAttributeNode(attrType1);
	         carname1.appendChild(doc.createTextNode(rs.getString(q1)));
	         supercar.appendChild(carname1);

	         Element carname2 = doc.createElement("Coloana");
	         Attr attrType2 = doc.createAttribute("_");
	         attrType2.setValue(q2);
	         carname2.setAttributeNode(attrType2);
	         carname2.appendChild(doc.createTextNode(rs.getString(q2)));
	         supercar.appendChild(carname2);
	         
	         Element carname3 = doc.createElement("Coloana");
	         Attr attrType3 = doc.createAttribute("_");
	         attrType3.setValue(q3);
	         carname3.setAttributeNode(attrType3);
	         carname3.appendChild(doc.createTextNode(rs.getString(q3)));
	         supercar.appendChild(carname3);
	         
	         

	         }
	         st.close();
	         // write the content into xml file
	         TransformerFactory transformerFactory = TransformerFactory.newInstance();
	         Transformer transformer = transformerFactory.newTransformer();
	         DOMSource source = new DOMSource(doc);
	         StreamResult result = new StreamResult(new File(tabel+".xml"));
	         transformer.transform(source, result);
	         
	         // Output to console for testing
	         StreamResult consoleResult = new StreamResult(System.out);
	         transformer.transform(source, consoleResult);
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	}
	
	
public static void main (String[] args){    
  JFrame frame = new JFrame("Baza de date");
  frame.setVisible(true);
  frame.setSize(500,200);
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

  JPanel panel = new JPanel();
  frame.add(panel);
  JButton button = new JButton("Generare XML");
  panel.add(button);
  button.addActionListener (new Action1());
}
static class Action1 implements ActionListener {        
  public void actionPerformed (ActionEvent e) {  
	  
	  JFrame frame = new JFrame("Baza de date");
	  frame.setVisible(true);
	  frame.setSize(500,200);
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  
	  JPanel panel = new JPanel();
	  frame.add(panel);
	  
	  
	  
	  JButton button1 = new JButton("Generale XML avion");
	  panel.add(button1);
	  button1.addActionListener (new Action3());
	  
	  JButton button2 = new JButton("Generale XML bilet");
	  panel.add(button2);
	  button2.addActionListener (new Action4());
	  
	  JButton button3 = new JButton("Generale XML ruta");
	  panel.add(button3);
	  button3.addActionListener (new Action5());
  }
}   
  
static class Action3 implements ActionListener {        
	  public void actionPerformed (ActionEvent e) {     
	    JFrame frame = new JFrame("Generare XML avion");
	    frame.setVisible(true);
	    frame.setSize(200,200);
	    xml("persoanehotel");
	    JLabel label = new JLabel("Fisierul XML a fost generat");
	    JPanel panel = new JPanel();
	    frame.add(panel);
	    panel.add(label);
	  }
	}
static class Action4 implements ActionListener {        
	  public void actionPerformed (ActionEvent e) {     
	    JFrame frame = new JFrame("Generare XML bilet");
	    frame.setVisible(true);
	    frame.setSize(200,200);
	    xml("personalconducere");
	    JLabel label = new JLabel("Fisierul XML a fost generat");
	    JPanel panel = new JPanel();
	    frame.add(panel);
	    panel.add(label);
	  }
	}
static class Action5 implements ActionListener {        
	  public void actionPerformed (ActionEvent e) {     
	    JFrame frame = new JFrame("Generare XML ruta");
	    frame.setVisible(true);
	    frame.setSize(200,200);
	    xml("personalservire");
	    JLabel label = new JLabel("Fisierul XML a fost generat");
	    JPanel panel = new JPanel();
	    frame.add(panel);
	    panel.add(label);
	  }
	}
}
